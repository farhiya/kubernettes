# Kubernettes

It is a way to manage and deploy containers at scale.
Works like terraform but for containers, with some extra features.

Includes:

- Self healing
- Monitoring
- Loved by the community

## How does it work?

K8 is setup as cluster (like ansible)
- k8 control panel
- k8 worker nodes
    - namespaces (like a vpc)
    - Services (endpoint for pods, like lb)
    - Pods (container + launch templates)


## Pre requisites and pre install notes

There are different ways to get a k8 cluster. It's generally a bit tricky to set up by yourself.

The following are different options:
  - Use kubespray (ansible)
  - kubeadm

Other services of kubernettes that are managed:
  - EKS (AWS) 
  - GKE (google)

There are also differnt distros of K8:
  - k8 offical
  - minikube
  - MicroK8s


## Setup

We're going to use kubespray

We're going to need:
  - 1 k8 control panel
  - 2 k8 worker nodes
  - 1 ansible machine with the right access

Minimum requirements mention:
- 16 GiB ram
- 4 cpu

each machine is t3a.large

```bash

sudo apt update
sudo apt install git -y
sudo apt install python3-pip -y
git clone https://github.com/kubernetes-sigs/kubespray.git
cd kubespray
sudo pip3 install -r requirements.txt
cp -rfp inventory/sample inventory/mycluster

# change the inventory file in kubespray/inventory/mycluster to:

# ## Configure 'ip' variable to bind kubernetes services on a
# ## different ip than the default iface
# ## We should set etcd_member_name for etcd cluster. The node that is not a etcd member do not need to set the value, or can set the empty string value.
[all]
node1 ansible_host=172.31.22.107 ip=172.31.22.107 etcd_member_name=etcd1 ansible_ssh_private_key_file=/home/ubuntu/.ssh/k8_keys 
node2 ansible_host=172.31.24.201 ip=172.31.24.201 etcd_member_name=etcd2 ansible_ssh_private_key_file=/home/ubuntu/.ssh/k8_keys 
node3 ansible_host=172.31.29.88  ip=172.31.29.88 etcd_member_name=etcd3 ansible_ssh_private_key_file=/home/ubuntu/.ssh/k8_pair 
# node4 ansible_host=95.54.0.15  # ip=10.3.0.4 etcd_member_name=etcd4
# node5 ansible_host=95.54.0.16  # ip=10.3.0.5 etcd_member_name=etcd5
# node6 ansible_host=95.54.0.17  # ip=10.3.0.6 etcd_member_name=etcd6

# ## configure a bastion host if your nodes are not directly reachable
# [bastion]
# bastion ansible_host=x.x.x.x ansible_user=some_user

[kube_control_plane]
node1
# node2
# node3

[etcd]
node1
node2
node3

[kube_node]
node2
node3
# node4
# node5
# node6

[calico_rr]

[k8s_cluster:children]
kube_control_plane
kube_node
calico_rr
#########################

# generate a key thats added to the 3 nodes manually 

cd ~/.ssh
ssh-add keygen

# save key to the name you want
# ssh into each node machine, cd into .ssh and add the new public key to the authorized_keys file


# Now go back into your ansible machine and run the following commands

# IPS=(private ip of control node private ip of worker node 1 private ip of worker node 2 )
declare -a IPS=(172.31.22.107 172.31.24.201 172.31.29.88)
CONFIG_FILE=inventory/mycluster/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}
cd ~/kubespray

# had to run several times to get all the files, cause we used smaller machines
ansible-playbook -i inventory/mycluster/hosts.yaml  --become --become-user=root cluster.yml

```

## Tasks

### Task 1 NAMESAPCES

What are name spaces?

In computing, a namespace is a set of signs that are used to identify and refer to objects of various kinds. A namespace ensures that all of a given set of objects have unique names so that they can be easily identified. Namespaces are commonly structured as hierarchies to allow reuse of names in different contexts.

had to config kubectl with the following:
```bash
sudo cp /etc/kubernetes/admin.conf $HOME/
sudo chown $(id -u):$(id -g) $HOME/admin.conf
export KUBECONFIG=$HOME/admin.conf
```

Check existing NAMESPACES
`kubectl get ns`

Make a new NS
`kubectl create namespace <name>`
Make another!
Delete a NS. How cool!
`kubectl delete namespace <name>`

### Task 2 DEPLOYMENT FILES - Now we need to deploy some pods (these have containers)

What is a deployment file?
  To specify which application modules will run in which containers, along with any container connections, module parameters, or trace instructions to configure at application start time.

what is the language?
  YAML

What "makes it a deployment file"
  when you run it, it creats container/pods etc


What are the 3 sections?
  - api version
  - kind
  - metada

Why are labels important?
  Kubernetes labels are key-value pairs that can connect identifying metadata with Kubernetes objects.  

Where do you specify the container IMAGE to use?
  under metadata, when defining container in template

How do you run a deploy file into your deployment file into your new Namesapce?
  

How do your check how many pods are running?
  `kubectl get pods`

How do you kill a pod?
  `kubectl delete pod <pod-name>`

how do you describe a pod?
  `kubectl describe pod <pod-name>`

What extra information do you get?
  - start time
  - lables
  - status
  - IP
  - CONTAINER INFO

How do you ssh into a pod?
  `kubectl exec --stdin --tty shell-<POD-NAME> -- /bin/bash`

How do you modify a deployment setup?

change the Image?

Change the number of pods?


### Task 3 Service Files

create a cluster service file
  `kubectl run nginx --image=nginx --restart=Never`
it needs to target the webapp you just created
Now how do you check your services running?
  `kubectl apply -f nginx-deployment.yaml -n <ns-name>`

Curl the serive

What other main services exist?
Task 4 document all your commands

