# Kubernetes 101 

It is a way to manage and deploy containers at scale! Works like terraform but for containers, with some extra features. 

Includes:

- Self healing
- Monioroing
- Loved by the community

### How does it work?

K8 is setup as cluster (like ansible).

- k8 Control panel
- k8 worker nodes

Inside the nodes you have:

- Namespaces (like a vpc)
- Services (endpoinds for pods, like LB)
- Pods (container + launch templates)

### Pre requisits And pre install notes

There are different ways to get a k8 cluster. It's generally a bit tricky to set up by yourself. The following are options

https://kubernetes.io/docs/setup/production-environment/tools/

- Use kubespray (ansible)
- kubeadm

Other serives of kubernetes that are managed:

- aws EKS
- Google GKE

Different distro of K8:

- k8 offical
- minikube
- MicroK8s


### our setup 

We'll use kubespray:

- https://github.com/kubernetes-sigs/kubespray

We're going to need:

- 1 k8 control panel - t3a.xlarge
- 2 k8 woker nodes  - t3a.xlarge
- 1 ansible machine with the right access t3a.xlarge


1. Creat all your instances
2. ssh into the ansible machine
3. git clone the kubespray
4. intall the requierments
5. copy the inventory and add the need private ips of the Control panel + nodes
6. Allow networking between the machines
7. generate a new key pair for the ansible to conect to the Control panel + nodes
8. Add they public key to autorized_keys of Control panel + nodes



## Setup Steps

### 1) Start Machines

We need 4 machines t3a.xlarge. Starte these and give them apropriate tags:

- Ansible
- k8 control panel
- k8 Agent A
- k8 Agent B

### 2) Allow SG networking

All machines:

- Allow cohort_9_home_ips

Ansible will have to connect to k8 machines to setup.

- Allow ansible into these machines - we can just add a group that allows cohort9_home_ips

### 3) Create and Share Key Pairs

In the Ansible Machine:

- Create a new key pair

```bash

$ ssh-keygen 
> Add a /path/name 
> no password

```

Loggin to our k8 machines:

- Add/append the public key pair to the authorized_keys file

Test step 3 and 4 by trying to loggin to each k8 machine via the ansible machine.

### 4) Clone Kubesprays to ansible machine

**Note** We migh need these dependencies:

```bash

# python3 
# git

```

Git clone the kubespray repo

```bash

$ git clone https://github.com/kubernetes-sigs/kubespray.git

```

Run the requierments.txt 

```bash

# Install dependencies from ``requirements.txt``
sudo pip3 install -r requirements.txt

```

### 5) Edit Inventory File in Kubespray & other setup

Kubespray has a sample inventory file we can copy and then edit.

```bash
# Copy ``inventory/sample`` as ``inventory/mycluster``
cp -rfp inventory/sample inventory/mycluster
```

On the inventory file add you k8s private IPs and specify the ssh. SSH key can also be specified when calling the ansible playbook.

```YAML
## Copy of your invenotry yaml from my cluster here

```

Then do this

```bash

# Update Ansible inventory file with inventory builder
declare -a IPS=(172.31.19.27  172.31.27.210 172.31.27.35)
CONFIG_FILE=inventory/mycluster/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}

# Review and change parameters under ``inventory/mycluster/group_vars``
cat inventory/mycluster/group_vars/all/all.yml
cat inventory/mycluster/group_vars/k8s_cluster/k8s-cluster.yml

```

### 6) Run Playbook with inventory File


